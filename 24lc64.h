
/* 
 * File:   24lc64.h
 * Author: Sethu
 *
 * Created on 28 June, 2014, 8:12 AM
 */

#ifndef 24LC64_H
#define	24LC64_H

// Function Declarations
void write_24lc64 (unsigned int addr, unsigned char data);
unsigned char read_24lc64 (unsigned int addr);
/*
void Write_Byte_To_24LC64_EEPROM(unsigned int, unsigned char);
unsigned char Read_Byte_From_24LC64_EEPROM(unsigned int);
void Write_Page_To_24LC64_EEPROM(unsigned int, unsigned char*, unsigned char);
void Read_Bytes_From_24LC64_EEPROM(unsigned int, unsigned char*, unsigned int);
*/
#endif	/* 24LC64_H */

