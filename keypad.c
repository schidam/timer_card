#include <xc.h>
#include "uart.h"
#include "functions.h"
//int keypad_map = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
int debounce_time;
void init_keypad() {
    TRISB = 0b11111001;
    debounce_time = 50;
}
int findKey(int a, int b) {//generating key character
    if(b == 0)
 {
   if(a == 1)
    return 1;
   else if(a == 0)
    return 0;
 }
 else if(b == 1)
 {
   if(a == 1)
    return 3;
   else if(a == 0)
    return 2;
 }
 else if(b == 2)
 {
   if(a == 1)
    return 5;
   else if(a == 0)
    return 4;
 }
 else if(b == 3)
 {
   if(a == 1)
    return 7;
   else if(a == 0)
    return 6;
  }
}

int readKeyboard() {
    unsigned int i = 0;
    for(i=0;i<4;i++) {
        if(i == 0)  PORTB = 4;
        else if(i == 1)   PORTB = 2;
        if(RB4)
           return findKey(i,0);
        if(RB5)
           return findKey(i,1);
        if(RB6)
           return findKey(i,2);
        if(RB7)
           return findKey(i,3);
    }
    return 10;
}

int getKey() {
    int f = 0;
    int temp;
    while( readKeyboard() == 10 ); // Wait for key Press (10 is the default return value in this case)
    f = readKeyboard(); //Read Pressed Key
    while(readKeyboard() == f); //Debounce (Wait for release and default value to be read)
    return f;
}

int get4digit() {
    int temp;
    int result = 0;
    int i=0;
    for (i=3;i>=0;i--) {
        temp = getKey();
        uart1send(temp);
        result+=temp*power(10,i);
    }
    return result;
}

