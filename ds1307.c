#include <xc.h>
#include "i2c.h"
//i2c 2
void init_DS1307 (void) {    /* Buffer where we will read/write our data */
    unsigned char I2CData[] = {0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x00, 0x00};
    unsigned char i;
//  I2CInit();  /* Initialize I2C Port */
    I2C2Start(); /* Send Start condition */
    I2C2Send(0xD0);  /* Send DS1307 slave address with write operation */
    I2C2Send(0x00);  /* Send subaddress 0x00, we are writing to this location */
    /* Loop to write 8 bytes */
    for (i = 0; i < 8; i++) {
            /* send I2C data one by one */
            I2C2Send(I2CData[i]);
    }
    /* Send a stop condition - as transfer finishes */
    I2C2Stop();
}

unsigned char read_DS1307 ( unsigned int addr) {
    /* We will now read data from DS1307 */
    /* Reading for a memory based device always starts with a dummy write */
    /* Send a start condition */
    I2C2Start();
    /* Send slave address with write bit set*/
    I2C2Send(0xD0);
    I2C2Send(addr);  // Address to be read from
    /* Send a repeated start, after a dummy write to start reading */
    I2C2Restart();
    /* send slave address with read bit set */
    I2C2Send(0xD1);
    /* Loop to read 8 bytes from I2C slave */
    unsigned char data =  I2C2Read();
    I2C2Stop();
    return data;
}


unsigned long int DS1307_get_time() {
    I2C2Start();
    I2C2Send(0xD0);  //write
    I2C2Send(0x00);  //sec addr
    I2C2Restart();
    I2C2Send(0xD1);  //read
    unsigned char bcd_sec = (I2C2Read() & 0x7F);
    I2C2Ack();
    unsigned char bcd_min = (I2C2Read() & 0x3F);
    I2C2Ack();
    unsigned char bcd_hour = (I2C2Read() & 0x0F);
    I2C2Ack();
    unsigned char day = (I2C2Read() & 0x07) - 0x01;
    I2C2Nak();
    I2C2Stop();
    unsigned char dec_sec = (bcd_sec & 0x0F) + (bcd_sec>>4)*10 ;
    unsigned char dec_min = (bcd_min & 0x0F) + (bcd_min>>4)*10 ;
    unsigned long int time = day*86400 + bcd_hour*3600 + dec_min*60 + dec_sec;
    return time;
}