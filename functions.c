#include <xc.h>
#include "define.h"

void delay (unsigned long int i) {
    unsigned int j=0;
    while(j++ < i) asm("nop");
}
void enable_interrupts() {
    GIE = 1;
    PEIE = 1;
}
void disable_interrupts() {
    GIE = 0;
    PEIE = 0;
}
void timer1_init() {
    T1CON = 0x01;
    T1GCONbits.TMR1GE = 0;
}

void timer1_start() {
    //timer 1 specific
    TMR1IE = 1;     //enable timer 1
    TMR1IF = 0;     //clear Timer 1 interrupt flag
}

void timer1_stop() {
    //timer 1 specific
    TMR1IE = 0;     //disable timer 1
    TMR1IF = 0;     //clear Timer 1 interrupt flag
}

void program_exit() {
    REL1 = 0;
    REL2 = 0;
    REL3 = 0;
    REL6 = 1;
}

int power(int a, int b) {
    int temp = 1;
    while(b--) {
        temp*=a;
    }
    return temp;
}
