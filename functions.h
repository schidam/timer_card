/* 
 * File:   functions.h
 * Author: Sethu
 *
 * Created on 23 July, 2014, 6:09 PM
 */

#ifndef FUNCTIONS_H
#define	FUNCTIONS_H

#ifdef	__cplusplus
extern "C" {
#endif

    void delay (unsigned long int);
    void enable_interrupts();
    void disable_interrupts();
    void timer1_init();
    void timer1_start();
    void timer1_stop();
    void program_exit();
    int power(int a, int b);

#ifdef	__cplusplus
}
#endif

#endif	/* FUNCTIONS_H */

