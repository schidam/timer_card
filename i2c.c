#include <xc.h>

/*

Function: I2CInit1
Description: Initialize I2C 1 in master mode, Sets the required baudrate

*/
void I2C1Init(void) {
	TRISC3 = 1;      /* SDA and SCL as input pin */
	TRISC4 = 1;      /* these pins can be configured either i/p or o/p */
	SSP1STAT |= 0x80; /* Slew rate disabled */
	SSP1CON1 = 0x28;   /* SSPEN = 1, I2C Master mode, clock = FOSC/(4 * (SSPADD + 1)) */
	SSP1ADD = 0x28;    /* 100Khz @ 4Mhz Fosc */
}

/*

Function: I2CStart
Return:
Arguments:
Description: Send a start condition on I2C Bus
*/
void I2C1Start()
{
	SSP1CON2bits.SEN = 1;         /* Start condition enabled */
	while(SSP1CON2bits.SEN);      /* automatically cleared by hardware */
                     /* wait for start condition to finish */
}

/*
Function: I2CStop
Return:
Arguments:
Description: Send a stop condition on I2C Bus

*/
void I2C1Stop()
{
	SSP1CON2bits.PEN = 1;         /* Stop condition enabled */
	while(SSP1CON2bits.PEN);      /* Wait for stop condition to finish */
                     /* PEN automatically cleared by hardware */
}

/*
Function: I2CRestart
Return:
Arguments:
Description: Sends a repeated start condition on I2C Bus

*/
void I2C1Restart()
{
	SSP1CON2bits.RSEN = 1;        /* Repeated start enabled */
	while(SSP1CON2bits.RSEN);     /* wait for condition to finish */
}


/*
Function: I2CAck
Return:
Arguments:
Description: Generates acknowledge for a transfer
*/
void I2C1Ack()
{
	SSP1CON2bits.ACKDT = 0;       /* Acknowledge data bit, 0 = ACK */
	SSP1CON2bits.ACKEN = 1;       /* Ack data enabled */
	while(SSP1CON2bits.ACKEN);    /* wait for ack data to send on bus */

}

/*
Function: I2CNck
Return:
Arguments:
Description: Generates Not-acknowledge for a transfer
*/
void I2C1Nak()
{
	SSP1CON2bits.ACKDT = 1;       /* Acknowledge data bit, 1 = NAK */
	SSP1CON2bits.ACKEN = 1;       /* Ack data enabled */
	while(SSP1CON2bits.ACKEN);    /* wait for ack data to send on bus */

}

/*
Function: I2CWait
Return:
Arguments:
Description: wait for transfer to finish

*/
void I2C1Wait()
{
	while ((SSP1CON2 & 0x1F ) || ( SSP1STAT & 0x04 ) );    /* wait for any pending transfer */
}

/*
Function: I2CSend
Return:

Arguments: dat - 8-bit data to be sent on bus
           data can be either address/data byte
Description: Send 8-bit data on I2C bus
*/

void I2C1Send(unsigned int dat)
{
	SSP1BUF = dat;    /* Move data to SSPBUF */
	while(SSP1STATbits.BF);       /* wait till complete data is sent from buffer */
	I2C1Wait();       /* wait for any pending transfer */
}

/*
Function: I2CRead

Return:    8-bit data read from I2C bus
Arguments:
Description: read 8-bit data from I2C bus
*/
unsigned char I2C1Read(void) {

	unsigned char temp;
/* Reception works if transfer is initiated in read mode */
	SSP1CON2bits.RCEN = 1;        /* Enable data reception */
	while(!SSP1STATbits.BF);      /* wait for buffer full */
	temp = SSP1BUF;   /* Read serial buffer and store in temp register */
	I2C1Wait();       /* wait to check any pending transfer */
	return temp;     /* Return the read data from bus */
}

/*

Function: I2CInit1
Description: Initialize I2C 1 in master mode, Sets the required baudrate

*/
void I2C2Init(void) {
	TRISD5 = 1;      /* SDA and SCL as input pin */
	TRISD6 = 1;      /* these pins can be configured either i/p or o/p */
	SSP2STATbits.SMP = 1; /* Slew rate disabled */
	SSP2CON1 = 0x28;   /* SSPEN = 1, I2C Master mode, clock = FOSC/(4 * (SSPADD + 1)) */
	SSP2ADD = 0x28;    /* 100Khz @ 4Mhz Fosc */
}

/*

Function: I2CStart
Return:
Arguments:
Description: Send a start condition on I2C Bus
*/
void I2C2Start()
{
	SSP2CON2bits.SEN = 1;         /* Start condition enabled */
	while(SSP2CON2bits.SEN);      /* automatically cleared by hardware */
                     /* wait for start condition to finish */
}

/*
Function: I2CStop
Return:
Arguments:
Description: Send a stop condition on I2C Bus

*/
void I2C2Stop()
{
	SSP2CON2bits.PEN = 1;         /* Stop condition enabled */
	while(SSP2CON2bits.PEN);      /* Wait for stop condition to finish */
                     /* PEN automatically cleared by hardware */
}

/*
Function: I2CRestart
Return:
Arguments:
Description: Sends a repeated start condition on I2C Bus

*/
void I2C2Restart()
{
	SSP2CON2bits.RSEN = 1;        /* Repeated start enabled */
	while(SSP2CON2bits.RSEN);     /* wait for condition to finish */
}


/*
Function: I2CAck
Return:
Arguments:
Description: Generates acknowledge for a transfer
*/
void I2C2Ack()
{
	SSP2CON2bits.ACKDT = 0;       /* Acknowledge data bit, 0 = ACK */
	SSP2CON2bits.ACKEN = 1;       /* Ack data enabled */
	while(SSP2CON2bits.ACKEN);    /* wait for ack data to send on bus */

}

/*
Function: I2CNck
Return:
Arguments:
Description: Generates Not-acknowledge for a transfer
*/
void I2C2Nak()
{
	SSP2CON2bits.ACKDT = 1;       /* Acknowledge data bit, 1 = NAK */
	SSP2CON2bits.ACKEN = 1;       /* Ack data enabled */
	while(SSP2CON2bits.ACKEN);    /* wait for ack data to send on bus */

}

/*
Function: I2CWait
Return:
Arguments:
Description: wait for transfer to finish

*/
void I2C2Wait()
{
	while ((SSP2CON2 & 0x1F ) || ( SSP2STAT & 0x04 ) );    /* wait for any pending transfer */
}

/*
Function: I2CSend
Return:

Arguments: dat - 8-bit data to be sent on bus
           data can be either address/data byte
Description: Send 8-bit data on I2C bus
*/

void I2C2Send(unsigned int dat)
{
	SSP2BUF = dat;    /* Move data to SSPBUF */
	while(SSP2STATbits.BF);       /* wait till complete data is sent from buffer */
	I2C2Wait();       /* wait for any pending transfer */
}

/*
Function: I2CRead

Return:    8-bit data read from I2C bus
Arguments:
Description: read 8-bit data from I2C bus
*/
unsigned char I2C2Read() {
	unsigned char temp;
/* Reception works if transfer is initiated in read mode */
	SSP2CON2bits.RCEN = 1;        /* Enable data reception */
	while(!SSP2STATbits.BF);      /* wait for buffer full */
	temp = SSP2BUF;   /* Read serial buffer and store in temp register */
	I2C2Wait();       /* wait to check any pending transfer */
	return temp;     /* Return the read data from bus */
}


