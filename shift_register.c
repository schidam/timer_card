#include <xc.h>
#include "functions.h"


#define SHCP_pin1 LATAbits.LATA0
#define STCP_pin1 LATAbits.LATA2

#define SHCP_pin2 LATAbits.LATA3
#define STCP_pin2 LATAbits.LATA5


int decode (char ch) {    //(Common Anode 7 Segment LED) (A wired to Q0, B to Q1, C to Q2...G to Q6)
    switch (ch) {           //(decimal to Q7)
        case '0': return 0b11000000; break;
        case '1': return 0b11111001; break;
        case '2': return 0b10100100; break;
        case '3': return 0b10110000; break;
        case '4': return 0b10011001; break;
        case '5': return 0b10010010; break;
        case '6': return 0b10000010; break;
        case '7': return 0b11111000; break;
        case '8': return 0b10000000; break;
        case '9': return 0b10010000; break;
        case 'A': case 'a': return 0b10001000; break;
        case 'B': case 'b': return 0b10000011; break;
        case 'C': case 'c': return 0b11000110; break;
        case 'D': case 'd': return 0b10100001; break;
        case 'E': case 'e': return 0b10000110; break;
        case 'F': case 'f': return 0b10001110; break;
        case 'G': case 'g': return 0b11000010; break;
        case 'H': case 'h': return 0b10001011; break;
        case 'I': case 'i': return 0b11111001; break;
        case 'J': case 'j': return 0b11110000; break;
        case 'K': case 'k': return 0b10001001; break;
        case 'L': case 'l': return 0b11000111; break;
        case 'N': case 'n': return 0b11001000; break;
        case 'O': case 'o': return 0b11000000; break;
        case 'P': case 'p': return 0b10001100; break;
        case 'Q': case 'q': return 0b10011000; break;
        case 'R': case 'r': return 0b10101111; break;
        case 'S': case 's': return 0b10010010; break;
        case 'T': case 't': return 0b10000111; break;
        case 'U': case 'u': return 0b11000001; break;
        case 'V': case 'v': return 0b11100011; break;
        case 'Y': case 'y': return 0b10010001; break;
        case 'Z': case 'z': return 0b10101100; break;
        default: return 0b11111111; break;
    }
}

void init_74HC595() {  // Call this in your main to setup the port.
    TRISEbits.TRISE0 = 0;  // TRIS for DS1
    TRISEbits.TRISE1 = 0;  // TRIS for DS2
    TRISEbits.TRISE2 = 0;  // TRIS for DS3
    TRISEbits.TRISE3 = 0;  // TRIS for DS4
    TRISEbits.TRISE4 = 0;  // TRIS for DS5
    TRISEbits.TRISE5 = 0;  // TRIS for SHCP_pin
    TRISEbits.TRISE6 = 0;  // TRIS for STCP_pin

}


void write_74HC595_1(char ch, char num) { //character,row
    int databyte = decode(ch);
    STCP_pin1 = 0; // Set the latch pin low
    for(int databits=0; databits<8; databits++) {
        if(databyte & 0b10000000)   { // Set the data pin high
            if(num == '1') { LATAbits.LATA1 = 1; }//LATA |= 0b00011110; }
            else if(num == '2') { LATEbits.LATE1 = 1; LATE |= 0b00011101; }
            else if(num == '3') { LATEbits.LATE2 = 1; LATE |= 0b00011011; }
            else if(num == '4') { LATEbits.LATE3 = 1; LATE |= 0b00010111; }
            else if(num == '5') { LATEbits.LATE4 = 1; LATE |= 0b00001111; }
        }
        else { // Set the data pin low
            if(num == '1') { LATAbits.LATA1 = 0; }//LATA |= 0b00011110; }
            else if(num == '2') { LATEbits.LATE1 = 0; LATE |= 0b00011101; }
            else if(num == '3') { LATEbits.LATE2 = 0; LATE |= 0b00011011; }
            else if(num == '4') { LATEbits.LATE3 = 0; LATE |= 0b00010111; }
            else if(num == '5') { LATEbits.LATE4 = 0; LATE |= 0b00001111; }
        }
        SHCP_pin1 = 1; // Set SHCP High
        SHCP_pin1 = 0; // Set SHCP Low
        databyte <<= 1;
    }
    STCP_pin1 = 1; // Set the latch pin high
    delay(300);
}

void write_74HC595_2(char ch, char num) { //character,row
    int databyte = decode(ch);
    STCP_pin2 = 0; // Set the latch pin low
    for(int databits=0; databits<8; databits++) {
        if(databyte & 0b10000000)   { // Set the data pin high
            if(num == '1') { LATAbits.LATA1 = 1; }//LATA |= 0b00011110; }
            else if(num == '2') { LATAbits.LATA4 = 1; }// LATE |= 0b00011101; }
            else if(num == '3') { LATEbits.LATE2 = 1; LATE |= 0b00011011; }
            else if(num == '4') { LATEbits.LATE3 = 1; LATE |= 0b00010111; }
            else if(num == '5') { LATEbits.LATE4 = 1; LATE |= 0b00001111; }
        }
        else { // Set the data pin low
            if(num == '1') { LATAbits.LATA1 = 0; }//LATA |= 0b00011110; }
            else if(num == '2') { LATAbits.LATA4 = 0;}// LATE |= 0b00011101; }
            else if(num == '3') { LATEbits.LATE2 = 0; LATE |= 0b00011011; }
            else if(num == '4') { LATEbits.LATE3 = 0; LATE |= 0b00010111; }
            else if(num == '5') { LATEbits.LATE4 = 0; LATE |= 0b00001111; }
        }
        SHCP_pin2 = 1; // Set SHCP High
        SHCP_pin2 = 0; // Set SHCP Low
        databyte <<= 1;
    }
    STCP_pin2 = 1; // Set the latch pin high
    delay(300);
}