/* 
 * File:   mcp3221.h
 * Author: Sethu
 *
 * Created on 18 July, 2014, 11:17 PM
 */

#ifndef MCP3221_H
#define	MCP3221_H

#ifdef	__cplusplus
extern "C" {
#endif

    unsigned int read_mcp3221(int addr);


#ifdef	__cplusplus
}
#endif

#endif	/* MCP3221_H */

