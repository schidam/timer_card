A library of C functions for PIC peripheral interfacing. 

Contains code for i2c, uart, eeprom read/write, 7-segment led display, serial ADC interface, DAC interface, and other formatting functions.

74hc595 - 8-bit serial in parallel out shift register
24lc64 - 64kB serial eeprom
ds1307 - real Time Clock
mcp3221 - Serail Analog to Digital Converter
