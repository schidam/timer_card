#include <xc.h>
#include "i2c.h"
//i2c 2
void write_3bytes_24lc64 (unsigned int addr, unsigned long int data) { // Writes 3 bytes to address
    I2C2Start();
    I2C2Send(0xA0);
    I2C2Send(addr>>8);
    I2C2Send(addr & 0xFF);
    I2C2Send(data & 0xFF);
    I2C2Send(data>>8);
    I2C2Send(data>>16);
    I2C2Stop();
}

unsigned long int read_3bytes_24lc64 (unsigned int addr) { //Reads 3 bytes from address location
    I2C2Start();
    I2C2Send(0xA0);
    I2C2Send(addr>>8);  // Address to be read from
    I2C2Send(addr & 0xFF);
    I2C2Restart();
    I2C2Send(0xA1);
    char d =  I2C2Read();
    I2C2Ack();
    unsigned long int data = d + (I2C2Read()<<8);
    I2C2Ack();
    d = I2C2Read();
    data = data + ( d <<16);
    I2C2Nak();
    I2C2Stop();
    return data;
}

void eeprom_write_data(unsigned int addr, unsigned long int time, unsigned int V, unsigned int C, unsigned int AH) { //addr,t,V,C,AH
    I2C2Start();
    I2C2Send(0xA0);
    I2C2Send(addr>>8);  // Address to be read from
    I2C2Send(addr & 0xFF);

    I2C2Send(time & 0xFF); I2C2Send(time>>8); I2C2Send(time>>16);
    I2C2Send(V & 0xFF); I2C2Send(V>>8);
    I2C2Send(C & 0xFF); I2C2Send(C>>8);
    I2C2Send(AH & 0xFF); I2C2Send(AH>>8);
    I2C2Stop();
}
