/* 
 * File:   newmain.c
 * Author: Sethu
 *
 * Created on 15 July, 2014, 11:11 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "i2c.h"
#include "eeprom.h"
#include "ds1307.h"
#include "uart.h"
#include "mcp3221.h"
#include "functions.h"
#include "define.h"
#include "shift_register.h"
#include "keypad.h"

/*
 * 
 */



bit SAFE, LOCKED, START, RESET; //control flags
volatile int k = 0;
volatile char mode = 'A';
volatile unsigned int V, C, AH;
volatile unsigned int V_limit, C_limit, AH_limit, cycles;
volatile unsigned int on_time, off_time, rsp1, rsp2;
volatile int trip;
volatile unsigned long int t;
unsigned int recall;


void main() {
    SAFE = 1;
    LOCKED = 0;
    START = 0;
    RESET = 1;
    //Initialise peripherals
    uart1init();    //uart
    I2C1Init();     //i2c 1 // ADC1 and ADC2
    I2C2Init();     //i2c 2 //memory and Ds1307
    timer1_init();  //timer1

    enable_interrupts();
    // Keypad
    // Row 0 to 3 = RB0 to RB3, Col 0to3 = RB4 to RB7
    init_keypad();
    TRISF = 0x00;
    char i = 0;
    int tp = 5;
    unsigned int time;
    int total_cycles = 0;
    unsigned int t_time;
    int press = 0;
    while(1) {
        {
            uart1send('x');
            int f = get4digit();
            uart1send('y');
            int i;
            for (i=4;i>=1;i--) {
                int temp = (( f%power(10,i) ) - (f%power(10,i-1)))/power(10,i-1);
                uart1send(temp+48);
            }
            delay(10000);
        }

        
    }
}
    //Display Arja Name
/*    do {
        //Define pin for the keylock
        while(LOCKED) {
        //poll this pin until unlocked. Display "key lock"
        }
        while(!LOCKED) {

            //Get mode from keypad (Define function: something like mode = get_value())
            //Mode = Individual Mode (0) or Cyclic Mode (1)
            switch(mode) {

                case 0:
                    //INDIVIDUAL MODE

                    //Get ON TIME
                    //Get V_limit
                    //Get C_limit
                    //Get AH_limit
                    //Get Rest Period
                    //Get Recall Period
                    //wait for start command
                    while(!START) {
                        //do nothing until Start is HIGH
                        //Either use keypad for start or define new button
                        //in a different port, then update start. Initially START = 0
                        //Poll the pin, do not use interrupt
                        /*
                         * if (key_pressed) {
                         *      START = 1;
                         * }
                         */
/*                    }
                    while(SAFE) {
                        unsigned int addr = 0x00;
                        init_DS1307();
                        unsigned long int t,t0; t0 = 0;
                        t = DS1307_get_time();
                        timer1_start();
                        REL1 = 1;
                        //change to recall value
                        if (t != t0 && t%recall == 0) {
                            eeprom_write_data(addr,t,V,C,AH);//write time, voltage, current, AH to eeprom
                            addr = addr + 0x09; //increment address
                            //send to PC
                            
                            t0 = t;
                        }
                        if ( (t < (on_time + rsp1)) ) {
                            REL2 = 1;
                            REL3 = 1;
                        }

                        if (t > on_time + rsp1) {
                            program_exit();
                            break;
                        }
                        //display V,C,AH,Time
                    } //end while
                    break;

                case 1:
                    //CYCLIC MODE
                    //Get ON TIME
                    //Get OFF TIME
                    //Get Number of Cycles
                    //Get V_limit
                    //Get C_limit
                    //Get AH_limit
                    //Get RSP1 time
                    //Get RSP2 time
                    //Get Recall period
                    //wait for start command
                    while(!START) {
                        //do nothing until Start is HIGH
                        //Either use keypad for start or define new button
                        //in a different port, then update start. Initially START = 0
                        //Poll the pin, do not use interrupt
                        /*
                         * if (key_pressed) {
                         *      START = 1;
                         * }
                         */
/*                    }
                    t_time = on_time + rsp1 + tp + off_time + rsp2 + tp;

                    while(SAFE) {
                        unsigned int addr = 0x00;
                        init_DS1307();
                        unsigned long int t0 = 0;
                        t = DS1307_get_time();
                        timer1_start();
                        REL1 = 1;
                        if (t != t0 ) {
                            eeprom_write_data(addr,t,V,C,AH);
                            //write time, voltage, current, AH to eeprom
                            addr = addr + 0x09;
                            //increment address by number of bytes stored (calculate!!)
                            if(t%recall == 0) {
                                //send to PC
                            }
                            t0 = t;
                        }
                        time = t % t_time;
                        if(cycles > total_cycles) {
                            break;
                        }
                        if(time >= 0 && time <= on_time+rsp1) {    // ON_TIME + RSP1
                            REL1 = 0; REL2 = 1; REL3 = 1; REL4 = 0; REL5 = 0; REL6 = 0;
                        }
                        if(time > on_time && time <= (on_time+rsp1+tp) ){  //Transition Period
                            REL4 = 1; REL2 = 0; REL3 = 0;
                        }
                        if(time > (on_time+rsp1+tp) && time <= (on_time+rsp1+tp+off_time+rsp2)) { // OFF_Time + RSP2
                            REL4 = 0; REL5 = 1;
                        }
                        if(time > (on_time+rsp1+tp+off_time+rsp2) && time <= t_time) { // Transition Period 2
                            REL4 = 1; REL5 = 0;
                        }
                        //display V,C,AH,Time
                    } //end while
                    break;

                default:
                    //Incorrect Mode
                    break;
            } // end switch
            //stop timer
            timer1_stop();


            if(!SAFE) {
                switch (trip) { //Insert Trip Display
                    case 0x01:
                        //Display type of trip - Voltage Trip
                        //Trip the proper relay
                        break;
                    case 0x02:
                        //Display type of trip - Current Trip
                        //Trip the proper relay
                        break;
                    case 0x03:
                        //Display type of trip - AH Trip
                        //Trip the proper relay
                        break;
                    default:
                        //do nothing
                        break;
                }

            } //end if
        } //end lock while
//       ask if user wants to reset?
//       wait here until a key is pressed (poll)!!!
//       if (reset key is pressed) {
//         RESET = 0;
//         }    
    }while(!RESET);

} //end main


//Interrupt Service Routine
void interrupt isr() {
    if(TMR1IF) {
        TMR1IF = 0;
        V = read_mcp3221(ADC1); // Voltage Read from ADC1
        //Insert Voltage and Current Conversions (V = V*(3/40)/1000 and C = C*(3/40)/1000)
        // 3/40 = 0.075 = 75mV
        C = read_mcp3221(ADC2); // Current Read from ADC2
        switch(mode) {  //Ampere Hour Calculation
            case 0: //Individual Mode
                AH = C_limit * on_time;
                break;
            case 1: //Cyclic Mode
                AH = C_limit * t;
                break;
            default:
                AH = 0;
                break;
        }
        if (V > V_limit) {
            SAFE = 0;
            trip = 0x01;
        }
        else if (C > C_limit) {
            SAFE = 0;
            trip = 0x02;
        }
        else if (AH > AH_limit) {
            SAFE = 0;
            trip = 0x03;
        }
    }//end timer1 isr
}//end isr
*/


