#include <xc.h>

#define DS_pin RD0    // LAT for DS_pin
#define SHCP_pin RD1
#define STCP_pin RD2

int display (int ch) {    //(Common Anode 7 Segment LED) (A wired to Q0, B to Q1, C to Q2...G to Q6)
    switch (ch) {           //(decimal to Q7)
        case '0': return 0b11000000; break;
        case '1': return 0b11111001; break;
        case '2': return 0b10101100; break;
        case '3': return 0b10110000; break;
        case '4': return 0b10011001; break;
        case '5': return 0b10010010; break;
        case '6': return 0b10000010; break;
        case '7': return 0b11111000; break;
        case '8': return 0b10000000; break;
        case '9': return 0b10010000; break;
        case 'A': case 'a': return 0b10001000; break;
        case 'B': case 'b': return 0b10000011; break;
        case 'C': case 'c': return 0b11000110; break;
        case 'D': case 'd': return 0b10100001; break;
        case 'E': case 'e': return 0b10000110; break;
        case 'F': case 'f': return 0b10001110; break;
        case 'G': case 'g': return 0b11000010; break;
        case 'H': case 'h': return 0b10001011; break;
        case 'I': case 'i': return 0b11111001; break;
        case 'J': case 'j': return 0b11110000; break;
        case 'K': case 'k': return 0b10001001; break;
        case 'L': case 'l': return 0b11000111; break;
        case 'N': case 'n': return 0b11001000; break;
        case 'O': case 'o': return 0b11000000; break;
        case 'P': case 'p': return 0b10001100; break;
        case 'Q': case 'q': return 0b10011000; break;
        case 'R': case 'r': return 0b10101111; break;
        case 'S': case 's': return 0b10010010; break;
        case 'T': case 't': return 0b10000111; break;
        case 'U': case 'u': return 0b11000001; break;
        case 'V': case 'v': return 0b11100011; break;
        case 'Y': case 'y': return 0b10010001; break;
        case 'Z': case 'z': return 0b10101100; break;
        default: return 0xFF; break;
    }
}

void initalise_74HC595() {  // Call this in your main to setup the port.
    TRISCbits.TRISC0 = 0;  // TRIS for DS_pin
    TRISCbits.TRISC1 = 0;  // TRIS for SHCP_pin
    TRISCbits.TRISC2 = 0;  // TRIS for STCP_pin
}

void pulse_shift_clock() {
    SHCP_pin = 1; // Set SHCP High
    SHCP_pin = 0; // Set SHCP Low
}

void write_74HC595(int ch) {
    int databyte = display(ch);
    STCP_pin = 0; // Set the latch pin low
    for(int databits=0; databits<8; databits++) {
            if(databyte & 0b10000000)   {
                DS_pin = 1; // Set the data pin high
            }
            else {
                DS_pin = 0;
            }
        pulse_shift_clock();
        databyte <<= 1;
    }
    STCP_pin = 1; // Set the latch pin high
}
