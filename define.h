/* 
 * File:   define.h
 * Author: Sethu
 *
 * Created on 24 July, 2014, 9:54 PM
 */

#ifndef DEFINE_H
#define	DEFINE_H

#ifdef	__cplusplus
extern "C" {
#endif

#define ADC1 1 //mcp3221 address
#define ADC2 7

#define REL1 RF0
#define REL2 RF1
#define REL3 RF2
#define REL4 RF5
#define REL5 RF4
#define REL6 RF7



#ifdef	__cplusplus
}
#endif

#endif	/* DEFINE_H */

