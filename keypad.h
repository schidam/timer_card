/* 
 * File:   keypad.h
 * Author: Sethu
 *
 * Created on 14 December, 2014, 4:22 AM
 */

#ifndef KEYPAD_H
#define	KEYPAD_H

#ifdef	__cplusplus
extern "C" {
#endif
    void init_keypad();
    int findKey(int a, int b);
    int readKeyboard();
    int getKey();
    int get4digit();
    
#ifdef	__cplusplus
}
#endif

#endif	/* KEYPAD_H */

