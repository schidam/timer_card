#include <xc.h>
#include "i2c.h"

//i2c 1
//ADC
// Read the value from the ADC
// addr is the last unique digit in the mcp3221 ADC address. (4th bit)
unsigned int read_mcp3221(int addr) {
    I2C1Start();
    I2C1Send(0x91+(addr<<1));
    unsigned int j = 0;
    while(j++<750); //conversion delay
    unsigned int data = I2C1Read();
    I2C1Ack();
    data = (data<<8) + I2C1Read();
    I2C1Nak();
    I2C1Stop();
    return data;
}

