/* 
 * File:   shift_register.h
 * Author: Sethu
 *
 * Created on 25 July, 2014, 4:33 PM
 */

#ifndef SHIFT_REGISTER_H
#define	SHIFT_REGISTER_H

#ifdef	__cplusplus
extern "C" {
#endif

int decode (int ch);

void init_74HC595();

void write_74HC595_1(char ch, char num);
void write_74HC595_2(char ch, char num);



#ifdef	__cplusplus
}
#endif

#endif	/* SHIFT_REGISTER_H */

