/* 
 * File:   i2c.h
 * Author: Sethu Chidambaram
 * Thankss to Siddharth Chandrasekaran @ embedjoural.com
 *
 * Created on 7 June, 2014, 9:01 PM
 */

#ifndef I2C_H
#define	I2C_H

void I2C1Init(void);
void I2C1Start();
void I2C1Stop();
void I2C1Restart();
void I2C1Ack();
void I2C1Nak();
void I2C1Wait();
void I2C1Send(unsigned int dat);
unsigned char I2C1Read(void);


void I2C2Init(void);
void I2C2Start();
void I2C2Stop();
void I2C2Restart();
void I2C2Ack();
void I2C2Nak();
void I2C2Wait();
void I2C2Send(unsigned int dat);
unsigned char I2C2Read();
#endif	/* I2C_H */

