/* 
 * File:   eeprom.h
 * Author: Sethu
 *
 * Created on 12 July, 2014, 10:58 PM
 */

#ifndef EEPROM_H
#define	EEPROM_H

#ifdef	__cplusplus
extern "C" {
#endif

void write_3bytes_24lc64 (unsigned int addr, unsigned long int data);
unsigned long int read_3bytes_24lc64 (unsigned int addr);
void eeprom_write_data(unsigned int addr, unsigned long int time, unsigned int V, unsigned int C, unsigned int AH);

#ifdef	__cplusplus
}
#endif

#endif	/* EEPROM_H */

