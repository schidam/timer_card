#include <xc.h>
#include "functions.h"
void uart1init(void) {
    TRISC7 = 1;
    TRISC6 = 0;
    //SPBRG = 5;     // 115200 BRGH = 1; 11.0592 Mhz
    SP1BRGL = 103; //9600, brgh = 1, brg16 = 0, sync = 0
    SP1BRGH = 0;
    BAUD1CON = 0;
    TX1STA = 0x24;
    RC1IE = 1;
    RC1STA = 0x90;
}

void uart1send(char TX_BYTE) {
    TX1REG = TX_BYTE;
    while(!TX1STAbits.TRMT);
}


char uart1receive(void) {
    while(!RC1IF); //Waits for Reception to complete
    return RC1REG; //Returns the 8 bit data
}

void pc_write(unsigned long int time, unsigned int V, unsigned int C, unsigned int AH) {
    uart1send('A');
    uart1send(time & 0xFF); uart1send(time>>8); uart1send(time>>16);
    uart1send(V & 0xFF); uart1send(V>>8);
    uart1send(C & 0xFF); uart1send(C>>8);
    uart1send(AH & 0xFF); uart1send(AH>>8);
}