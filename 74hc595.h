/* 
 * File:   74hc595.h
 * Author: Sethu
 *
 * Created on 25 July, 2014, 2:01 AM
 */

#ifndef 74HC595_H
#define	74HC595_H

#ifdef	__cplusplus
extern "C" {
#endif

int display (int ch);
void initalise_74HC595();
void pulse_shift_clock();
void write_74HC595(int ch);


#ifdef	__cplusplus
}
#endif

#endif	/* 74HC595_H */

