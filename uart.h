/* 
 * File:   uart.h
 * Author: Sethu
 *
 * Created on 3 July, 2014, 9:48 AM
 */

#ifndef UART_H
#define	UART_H

#ifdef	__cplusplus
extern "C" {
#endif

void uart1init(void);
void uart1send(char TX_BYTE);
char uart1receive(void);
void pc_write(unsigned long int time, unsigned int V, unsigned int C, unsigned int AH);


#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

