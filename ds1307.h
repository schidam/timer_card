/* 
 * File:   ds1307.h
 * Author: Sethu
 *
 * Created on 28 June, 2014, 8:27 AM
 */

#ifndef DS1307_H
#define	DS1307_H

// Function Declarations
void init_DS1307 ( void );
unsigned char read_DS1307 ( unsigned int addr );
unsigned long int DS1307_get_time();

#endif	/* DS1307_H */

